# 20160916 Scott Havens
# 
# Load in the historic NRCS data

# add the metadata

python load_historic.py metadata

# data
python load_historic.py /media/spindrift/LT_BRB/SnotelData_Hourly/wy2001
python load_historic.py /media/spindrift/LT_BRB/SnotelData_Hourly/wy2002
python load_historic.py /media/spindrift/LT_BRB/SnotelData_Hourly/wy2003
python load_historic.py /media/spindrift/LT_BRB/SnotelData_Hourly/wy2004
python load_historic.py /media/spindrift/LT_BRB/SnotelData_Hourly/wy2005
python load_historic.py /media/spindrift/LT_BRB/SnotelData_Hourly/wy2006
python load_historic.py /media/spindrift/LT_BRB/SnotelData_Hourly/wy2007
python load_historic.py /media/spindrift/LT_BRB/SnotelData_Hourly/wy2008
python load_historic.py /media/spindrift/LT_BRB/SnotelData_Hourly/wy2009
python load_historic.py /media/spindrift/LT_BRB/SnotelData_Hourly/wy2010
python load_historic.py /media/spindrift/LT_BRB/SnotelData_Hourly/wy2011
python load_historic.py /media/spindrift/LT_BRB/SnotelData_Hourly/wy2012
python load_historic.py /media/spindrift/LT_BRB/SnotelData_Hourly/wy2013
python load_historic.py /media/spindrift/LT_BRB/SnotelData_Hourly/wy2014
python load_historic.py /media/spindrift/LT_BRB/SnotelData_Hourly/wy2015



